import { Common } from './../../prod/base/Common';
import { expect } from 'chai';
import 'mocha';

describe('Common: HashPassword Tests', () => {
    
    it('should return expected hash', () => {
        const result = Common.HashPassword('password');
        expect(result).to.equal('5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8');
    })

    it('should return expected hash for simple string', () => {
        const result = Common.HashPassword('1');
        expect(result).to.equal('6B86B273FF34FCE19D6B804EFF5A3F5747ADA4EAA22F1D49C01E52DDB7875B4B');
    })
})
