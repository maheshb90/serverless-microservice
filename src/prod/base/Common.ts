import { createHash } from "crypto";

export class Common {
    private static _passwordHashingAlgorithm: string = 'sha256';

    /**
     * Creates a Password hash 
     * @param password - User's password
     */
    public static HashPassword(password: string): string {
        /* TODO: Implement salting */
        return createHash(Common._passwordHashingAlgorithm)
                .update(password)
                .digest('hex')
                .toUpperCase();
    }
}