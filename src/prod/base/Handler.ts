/**
 * A base class for all handlers for executing some common logic 
 */ 

enum StatusCodes {
    Success = 200,
    AuthenticationFailure = 403,
    Error = 500
}

interface HandlerResponse {
    statusCode: StatusCodes,
    response: any 
}

class PhaseHandler {
    
}

export { StatusCodes, HandlerResponse, PhaseHandler }