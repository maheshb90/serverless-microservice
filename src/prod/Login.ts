import { Handler, Context, Callback } from 'aws-lambda';
import { PhaseHandler } from './base/Handler.js';
import { Common } from './base/Common.js';

class LoginHandler extends PhaseHandler {
    public Login:Handler = (event: any, context: Context, callback: Callback) => {
        const response: any = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Login successful!'
            })
        };        
        callback(undefined, response);
    }

    public ValidateInput(input: any): boolean {
        var result = false;
        
        if(input != null && 
            input.userName != null &&
            input.password != null) {
                result = true;
            }

        return result;
    }

    public AuthenticateUser(userName: string, password: string) {
        var hash = Common.HashPassword(password);
        
    }
}

export default new LoginHandler().Login;