# Serverless-Microservice

How about running n JWT authenticated REST API serveice using node.js on the AWS lambda service

## Build dependencies

1. **node.js 8.10.0** - node.js 8.10 is an older version, and is the only version, with major revision 8, supported officially by AWS lambda
    Make use of 'n' to manage different versions of nodejs in your development machine.
1. **Yarn/NPM** - Pacakge manager of your choice
1. **Docker** - Docker is a development dependency to manage and run the AWS Dynamo DB and AWS lambda runtime environments. 
    Make sure that the docker command can be run the current user and does not require sudo or any privilage elevations.

## How to build

Install the dependencies using the command

`npm install`

Build using the following command.

`npm run-script build`

All of development is done using typescript. This gives us the advantage of enforcing strong types in javascript. 

The repository is configured to transpile all the **.ts** code in the *src/* directory into javascript and store the output in **dist/** directory

To build and run the unit test cases, use the following command

`npm run-script test`

## Testing

Testing all this code has been planned in three different ways

### Unit testing

The obvious and verstaile mode of testing the codem, making use of **mocha** and **chai** library for test orchestration and assertions

### Integration testing

Making use of **express** to host the AWS lambda handlers as separate local endpoints and running tests against the hosted local server
The database is provided by running AWS DynamoDB local instance using a docker image.
In this method we can run all the handler code without actually needing to connect to the actual AWS Lambda service or run any special lambda docker image locally

### Staged testing (AWS)

Create a staging stack containing AWS API Gateway, Lambda, DynamoDB and S3 buckets and push all JS code for testing.

