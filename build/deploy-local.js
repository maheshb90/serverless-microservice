const { execSync } = require('child_process');

var Green   = "\x1b[32m";
var White   = "\x1b[37m";
var Red     = "\x1b[31m";
var Reset   = "\x1b[0m";

var commands = [
        { 
            command: "docker pull maheshb90/aws-dynamo-db", 
            display: "Pulling DynamoDB docker image"
        },
        { 
            command: "docker run -d -p 8000:8000 -v /tmp/data:/data/ maheshb90/aws-dynamo-db -dbPath /data/", 
            display: "Running DynamoDB docker image"
        }, 
        {
            command: "node build/localDBSetup.js",
            display: "Deploying database schema"
        }
]


commands.forEach(cmd => {
    console.log(White + " > " + cmd.display + Reset + " (" + cmd.command + ")");
    var out = execSync(cmd.command)
    console.log(out.toString());   
});