module.exports = [
    {
        table: 'User',
        data: {
            name: {  S: 'Mahesh Balakrishnan' },
            email: { S: 'maheshb90@live.com' },
            age: { N: '27' },
            phone: { S: '123-555-1233' },
            passwordHash: { S: 'ajkshd9yas9dhoasudh9ahsdona9s8dhkasdion' }
        }
    },
    {
        table: 'User',
        data: {
            name: {  S: 'Paul Pogba' },
            email: {  S: 'paul@live.com' },
            age: { N: '24' },
            phone: {  S: '123-555-1233' },
            passwordHash: {  S: 'ajkshd9yas9dhoasudh9ahsdona9s8dhkasdion' }
        }
    },
    {
        table: 'User',
        data: {
            name: {  S: 'Anthony martial' },
            email: {  S: 'martial@live.com' },
            age: { N: '22' },
            phone: {  S: '123-555-1233' },
            passwordHash: {  S: 'ajkshd9yas9dhoasudh9ahsdona9s8dhkasdion' }
        }
    },
    {
        table: 'AuthenticationToken',
        data: {
            email: {  S: 'martial@live.com' },
            token: {  S: '87yas87dha89sdh9aysd9ya9sdy98aysd9y9' },
            time: { N: '5633332445455' },
            ipaddress: {  S: '10.0.28 ' }
        }
    }
]