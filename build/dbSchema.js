module.exports = [
    {
        name: 'User',
        indexKeys: [
            {
                keyName: 'email',
                keyType: 'S',
                primary: true
            }
        ]        
    },
    {
        name: 'AuthenticationToken',
        indexKeys: [
            {
                keyName: 'token',
                keyType: 'S',
                primary: true
            },
            {
                keyName: 'email',
                keyType: 'S',
                primary: false
            }
        ]    
    }
];