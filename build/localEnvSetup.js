const { exec } = require('child_process');

var Green   = "\x1b[32m";
var White   = "\x1b[37m";
var Red     = "\x1b[31m";
var Reset   = "\x1b[0m";

pullDockerImage("maheshb90/environment", "AWS DynamoDB");

/**
 * Pulls a docker image from the internet
 * @param {Name of the image as per docker hub specification} imageName
 * @param {Display name for logging purposes} displayName 
 */
function pullDockerImage(imageName, displayName) {
    console.log(White + "Starting to download docker image: "+ displayName + ", Please wait..." + Reset)
    exec('docker pull ' + imageName, (err, stdout, stderr) => {
        if(err) {
            console.log(Red + "An error occurred while pulling docker image: " + displayName)
            console.log(stdout)
            console.log(err)
            console.log(stderr)
            console.log(Reset)
            return;
        }
        console.log(Green + "Success! Pulled down " + displayName + Reset)
    });
}