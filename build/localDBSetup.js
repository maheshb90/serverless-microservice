const AWS = require('aws-sdk');
const Tables = require('./dbSchema.js');
const DummyData = require('./dummyData.js');

if (typeof Promise === 'undefined') {
  AWS.config.setPromisesDependency(require('bluebird'));
}

var db = init();

db.listTables({}).promise().then(function(data) {
  if(data.TableNames.length > 0) {    
    return deleteAllTables(db, data).then(function(data) {
      return createAllTables(db, Tables);
    });
  } else {
    return createAllTables(db, Tables);
  }
})
.then(function(data) {
  return insertIntoTable(db, DummyData);
})
.then(function(data) {
  console.log("Testing deployment...")
  return getItem(db, 'User', 'email', 'S', 'maheshb90@live.com');
}).
then(function(data) {
  if(data !== null && data.Item.age.N === '27') {
    console.log('Pass.')
  } else {
    console.log("Something went wrong! Dumping data..");
    console.log(data);
  }
  
})
.catch(function(err) {
  handleError(err);
});

/**
 * Returns a promise to create specified tables
 * @param {DynamoDB database object} db 
 * @param {List of tables to create} tables 
 */
function createAllTables(db, tables) {
  console.log("Creating all tables..!");
  return Promise.all(tables.map(function(table) {
    return db.createTable(getCreateTableParamsTemplate(table)).promise();
  }));
}

/**
 * Returns a promise to delete all specified tables
 * @param {DynamoDB database connection Object} db 
 * @param {List of tables to delete} data 
 */
function deleteAllTables(db, tables) {
  console.log("Deleting all tables..!");
  return Promise.all(tables.TableNames.map(function(table) {
    return db.deleteTable({
            TableName: table
          }).promise();
  }));
}

/**
 * Returns a promise to describe the specified table
 * @param {DynamoDB database connection Object} db 
 * @param {Tables to fetch decription} data 
 */
function describeTable(db, table) {
  console.log("Fetching description of " + table + "..!");  
  return db.describeTable({
          TableName: table
          }).promise();
}
/**
 * Inserts a record into the database
 * @param {DynamoDB database object} db 
 * @param {Record to insert into database} record 
 */
function insertIntoTable(db, records) {
  return Promise.all(records.map(function(record){
    return db.putItem(getInsertItemParams(record)).promise();
  }));
}

/**
 * 
 * @param {*} db 
 * @param {*} table 
 * @param {*} keyName 
 * @param {*} keyType 
 * @param {*} keyValue 
 */
function getItem(db, table, keyName, keyType, keyValue) {
  var params = {};
  params[keyName] = {};
  params[keyName][keyType] = keyValue;
  return db.getItem({
    Key: params,
    TableName: table
  }).promise();
}

function handleError(err) {
  if(err) {
    console.log(err, err.stack);
    process.exit(1);
  }
}

/**
 * Initializes the connection to the local database
 */
function init() {
  var connConfig = {
      "apiVersion": "2012-08-10",
      "accessKeyId": "phaselocalaccess",
      "secretAccessKey": "phaselocalaccess",
      "region":"ap-south-1",
      "endpoint": "http://localhost:8000"
    }
    
  return new AWS.DynamoDB(connConfig);  
}

/**
 * Creates an AWS SDK compliant params object for table creation 
 * in dynamoDB
 * @param {Object describing the details of the table} tableDetails 
 */
function getCreateTableParamsTemplate(tableDetails) {
  var attributeDefinitions = tableDetails.indexKeys.map(function(index){
      return {
            AttributeName: index.keyName,
            AttributeType: index.keyType
          }              
    });  
  var keySchema = tableDetails.indexKeys.map(function(index){
    return {
        AttributeName: index.keyName,
        KeyType: index.primary ? "HASH" : "RANGE"
      }            
  });  
  
  return {
    AttributeDefinitions: attributeDefinitions,
    KeySchema: keySchema,
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5
    },
    TableName: tableDetails.name
  };
}

/**
 * 
 * @param {*} itemDetails 
 */
function getInsertItemParams(itemDetails) {
  return {
    Item: itemDetails.data, 
    ReturnConsumedCapacity: "TOTAL", 
    TableName: itemDetails.table
   };
}